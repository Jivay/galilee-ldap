# -*- coding: utf-8 -*-
###############################################################################
#       galilee_ldap.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bibliothèque d'appel au LDAP de Galilée"""

import os
import sys
import ldap
import json
from passlib.hash import ldap_salted_sha1 

# import de la configuration
sys.path.insert(0, '/etc/galilee_ldap')
import conf

# exceptions
class NumAdhInconnu(Exception):
    """Le numéro d'adhérent·e précisé est inconnu"""
    def __init__(self, numadh):
        Exception.__init__(self)
        self.numadh = numadh

class StructureInconnue(Exception):
    """Cette opération nécessite qu'une structure soit définie"""
    pass

# classe principale d'accès au LDAP
class GalileeLDAP:
    """Gestion des données LDAP d'une région ou structure de Galilée
    
    structure est l'identifiant de la région/structure concernée
    Attention : cet identifiant doit correspondre à la fois au nom
    dans le ldap, mais aussi au nom de domain en .eedf.fr pour les mails
    
    structure peut être '*', pour accéder aux données de toutes les structures.
    """
    
    def __init__(self, structure='*'):
        """Connexion en root au serveur LDAP"""
        self.ldap_connexion = ldap.initialize(conf.LDAP_URL)
        self.ldap_connexion.simple_bind_s(conf.LDAP_RDN, conf.LDAP_PW)
        
        self.structure = structure
        if structure == '*':
            self.filter_structure = ''
        else:
            self.filter_structure = 'ou={},'.format(structure)
        
    def dn(self, numadh):
        """Retourne le dn de l'adhérent·e `numadh`"""
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN, 
            ldap.SCOPE_SUBTREE, 
            'cn={}'.format(numadh), 
            ['cn'] 
        )
        if data:
            return data[0][0] # we assume results are uniq…
        else:
            return None
        # approche naïve
        #return 'cn={numadh},{f_structure}{bdn}'.format(
        #    numadh=numadh,
        #    f_structure=self.filter_structure,
        #    bdn=conf.LDAP_BDN,
        #)
    
    def adh_data(self, numadh, attrs=None):
        """Retourne les données de l'adhérent·e `numadh`
            Si attrs est une liste d'attributs, ne renvoie que ces attributs
        """
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN,
            ldap.SCOPE_SUBTREE,
            'cn={}'.format(numadh),
            attrs
        )
        if data:
            return self._bytes_to_unicode(data[0][1])
        else:
            raise NumAdhInconnu(numadh)

    def recherche_numadh(self, filtre):
        '''Renvoie les numéros d'adhérent·es correspondants au filtre.
        
        Exemple de filtre : (mail=test@example.com,uid=test)
        ''' 
        data = self.ldap_connexion.search_s(
            self.filter_structure + conf.LDAP_BDN,
            ldap.SCOPE_SUBTREE,
            filtre,
            ['cn']
        )
        if data:
            u_data = []
            for res in data:
                try:
                    u_data.append(self._bytes_to_unicode(res[1]['cn'][0]))
                except KeyError:
                    pass #no cn… we forget
            return u_data
        else:
            return []
        
    def creer_compte(self, numadh, login, pw, hashed=False):
        '''Créer un compte UNIX (et plus encore)
        numadh : le numéro de l'adhérent concerné
        login : le pseudonyme choisi
        pw : mot de passe
        hashed: si le mot de passe est déjà hashé (via la fonction make_secret).
                si ce n'est pas le cas, le mot de passe est hashé avant 
                insertion.
        '''
        def _get_ou_from_dn(dn):
            ou = [p for p in dn.split(',') if p.startswith('ou=')][0]
            return ou[3:]
        # récupérer infos sur l'adhérent·e
        moddn = self.dn(numadh)
        structure = _get_ou_from_dn(moddn)
        mail_domain = conf.STRUCTURES[structure][0]
        
        # check password
        if not hashed:
            pw = self.make_secret(pw)
        
        mod = [
            (ldap.MOD_ADD, 'objectClass','posixAccount'),
            (ldap.MOD_ADD, 'loginShell', '/bin/bash'),
            (ldap.MOD_ADD, 'homeDirectory','/home/'+login),
            (ldap.MOD_ADD, 'uidNumber', '{}'.format(int(numadh))),
            (ldap.MOD_ADD, 'gidNumber', '{}'.format(int(numadh))),
            (ldap.MOD_ADD, 'uid', login),
            (ldap.MOD_ADD, 'userPassword', pw),
            (ldap.MOD_ADD, 'mail', '{}@{}'.format(login, mail_domain)),
        ]
        mod = [
            (op, at, self._unicode_to_bytes(val)) for (op, at, val) in mod
        ]
        try:
            self.ldap_connexion.modify_s(moddn, mod)
        except ldap.CONSTRAINT_VIOLATION:
            return False
        else:
            return True
    
    def creer_compte_ext(self, nom, prenom, fonction, fake_numadh, mail,
            login, pw, hashed=False):
        """Créer une nouvelle entrée dans le LDAP associée à un compte UNIX,
            pour des personnes non-adhérent·es aux structures membres de
            Galilée.
            
            fake_numadh est un faux numéro d'adhérent·e, qui doit être unique.
                (aucune vérification n'est effectuée dans cette méthode)
            
            hashed: définit si le mot de passe est déjà hashé (via make_secret).
                si hashed=False, le mot de passe est hashé avant insertion.
        """
        # check password
        if not hashed:
            pw = self.make_secret(pw)
        
        # création du nouvel enregistrement
        guest=[
            ('objectClass',[
                'person',
                'organizationalPerson',
                'inetOrgPerson',
                'schacPersonalCharacteristics',
                'posixAccount', 
                'eedfAdherent'
            ]),
            ('cn', fake_numadh),
            ('sn', nom),
            ('givenName', prenom),
            ('title', fonction),
            ('mail', mail),
            ('uid', login),
            ('userPassword', pw),
            ('uidNumber', fake_numadh),
            ('gidNumber', fake_numadh),
            ('homeDirectory', '/home/'+login),
            ('loginShell', '/bin/bash'),
        ]
        guest = [
            (at, self._unicode_to_bytes(val)) for (at, val) in guest
        ]
        
        dn = 'cn={fake_numadh},ou={guest_ou},{bdn}'.format(
            fake_numadh = fake_numadh,
            guest_ou = conf.GUEST_OU,
            bdn = conf.LDAP_BDN
        )
        
        # ajouter la nouvelle entrée dans le LDAP
        self.ldap_connexion.add_s(dn, guest)

    def editer_adh(self, numadh, data):
        """Modifier une entrée 
        numadh : numéro d'adhérent·e dont l'entrée est à modifier
        data : [(nom_attribut, nouvelle_valeur),...]
            nouvelle_valeur peut être None pour supprimer l'attribut.
        """
        mod = [] 
        for key in data: 
            if key[1] is None: 
                op = ldap.MOD_DELETE
            else:
                op = ldap.MOD_REPLACE 
            mod.append((op, key[0], self._unicode_to_bytes(key[1])))
        self.ldap_connexion.modify_s(self.dn(numadh), mod)
    
    def dump(self, numadh_list, json_file):
        """Sauvegarde en json les données des adhérents de `numadh_list`"""
        data = []
        for numadh in numadh_list:
            data.append((
                self.dn(numadh),
                self.adh_data(numadh)
            ))
        with open(json_file, 'w') as json_fd:
            json_fd.write(json.dumps(data))


    ### Utilities

    def _bytes_to_unicode(self, obj):
        """Retourne une représentation unicode des bytes de obj"""
        if isinstance(obj, list):
            return [ self._bytes_to_unicode(i) for i in obj ]
        elif isinstance(obj, tuple):
            return tuple(( self._bytes_to_unicode(i) for i in obj ))
        elif isinstance(obj, dict):
            newdict = {}
            for key in obj:
                newdict[key] = self._bytes_to_unicode(obj[key])
            return newdict
        elif isinstance(obj, bytes):
            return obj.decode('utf-8')
        else:
            return obj

    def _unicode_to_bytes(self, obj):
        """Retourne une représentation bytes des textes de obj"""
        if isinstance(obj, list):
            return [ self._unicode_to_bytes(i) for i in obj ]
        elif isinstance(obj, tuple):
            return tuple(( self._unicode_to_bytes(i) for i in obj ))
        elif isinstance(obj, dict):
            newdict = {}
            for key in obj:
                newdict[key] = self._unicode_to_bytes(obj[key])
            return newdict
        elif isinstance(obj, str):
            return obj.encode('utf-8')
        else:
            return obj
    
    def make_secret(self, password):                                                       
        """                                                                          
        Encodes the given password as a base64 SSHA hash+salt buffer                 
        """
        return ldap_salted_sha1.hash(password)
        

