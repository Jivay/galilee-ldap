#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       galilee_ldap_shell.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-ldap.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Shell pour exécuter des commandes sur le LDAP de Galilée"""

import cmd
import shlex
import readline
import os.path
import argparse

histfile = os.path.expanduser('~/.galilee_ldap_shell.history')
histfile_size = 1000

class GalileeLDAPShell(cmd.Cmd):
    intro = '''Bienvenue sur le shell du LDAP de Galilée.
Entre 'help' ou '?' pour lister les commandes.
'''
    prompt = '(galilee_ldap)$ '
    
    def __init__(self, galilee_ldap_connector):
        cmd.Cmd.__init__(self)
        self.gldap = galilee_ldap_connector
    
    def do_exit(self, args=None):
        """Exit the shell"""
        return True
    do_EOF = do_exit

    def do_dn(self, numadh):
        """Retourne le dn d'un numéro d'adhérent·e"""
        try:
            print(self.gldap.dn(numadh))
        except NumAdhInconnu:
            print("Numéro d'adhérent·e inconu.")
        

    def do_adh_data(self, args):
        """Renvoie les données d'un·e adhérent·e
        Usage: adh_data numadh [attr1,attr2…]
        """
        arg_list = shlex.split(args)
        numadh = arg_list[0]
        if len(arg_list) > 1:
            attrs = arg_list[1].split(',')
        else:
            attrs = None
        try:
            print(self.gldap.adh_data(numadh, attrs))
        except NumAdhInconnu as nai:
            print("Numéro d'adhérent·e inconnu.")
    
    def do_recherche_numadh(self, filtre):
        """Recherche les numéros d'adhérent·es correspondant au filtre"""
        print(self.gldap.recherche_numadh(filtre))
    
    def do_editer_adh_data(self, args):
        """Modifier un attribut d'un·e adhérent·e
        usage: editer_adh_data numadh attr valeur
        """
        arg_list = shlex.split(args)
        try:
            numadh, attr, valeur = arg_list
        except ValueError:
            print("Mauvais nombre de paramètres. help editer_adh_data pour plus d'infos.")
            return
        
        try:
            self.gldap.editer_adh(numadh, [(attr, valeur)])
        except NumAdhInconnu:
            print("Numéro d'adhérent·e inconnu.")
    
    def do_creer_compte(self, args):
        """Créer un compte Galilée d'un·e adhérente·e
        usage: creer_compte numadh login password
        """
        try:
            numadh, login, pw = shlex.split(args)
        except ValueError:
            print("Mauvais nombre de paramètres. help creer_compte pour plus d'infos.")
            return
        
        self.gldap.creer_compte(numadh, login, pw)

    def do_creer_compte_ext(self, args):
        """Créer un compte Galilée d'une personne extérieure
        usage : creer_compte_ext nom prenom fonction fake_numadh mail login password
        """
        try:
            nom, prenom, fonction, fake_numadh, mail, login, password = shlex.split(args)
        except ValueError:
            print("Mauvais nombre de paramètres. help creer_compte_ext pour plus d'infos.")
            return
        
        self.gldap.creer_compte_ext(nom, prenom, fonction, fake_numadh, mail, 
                login, password)

    def do_json_dump(self, args):
        """Sauvegarde les données des adhérent·es listé·es dans un fichier json
        Usage: json_dump numadh1,numadh2 json_file
        """
        arg_list = shlex.split(args)
        try:
            numadh_list = arg_list[0].split(',')
            json_file = arg_list[1]
        except ValueError:
            print("Mauvais nombre de paramètres. help json_dump pour plus d'infos.")
            return
        
        try:
            self.gldap.dump(numadh_list, json_file)
        except NumAdhInconnu as nai:
            print("Numéro d'adhérent·e inconnu : {}.".format(nai.numadh))
    
    # history management
    def preloop(self):
        """Load history"""
        if readline and os.path.exists(histfile):
            readline.read_history_file(histfile)
                                    
    def postloop(self):
        """Write history"""
        if readline:
            readline.set_history_length(histfile_size)
            readline.write_history_file(histfile)
        
if __name__ == '__main__':
    # parse command line
    parser = argparse.ArgumentParser(
        description='Galilée LDAP shell')
    parser.add_argument('--from_json', metavar='JSON', 
        help="Instead of LDAP, use the fake lib with JSON file.")
    args = parser.parse_args()
    if args.from_json:
        from galilee_ldap_fake import FakeGalileeLDAP, NumAdhInconnu
        GalileeLDAPShell(FakeGalileeLDAP(args.from_json)).cmdloop()
    else:
        from galilee_ldap import GalileeLDAP, NumAdhInconnu
        GalileeLDAPShell(GalileeLDAP()).cmdloop()
