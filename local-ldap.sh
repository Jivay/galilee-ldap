#!/bin/sh

# Ce script permet la connexion locale au LDAP de Galilée sur une machine de
# test. Il nécessite d'avoir une clef SSH donnant l'accès à root

local_port=9389

echo "Connecté au LDAP, port local : $local_port. Control-C pour quitter."
ssh -N -L $local_port:localhost:389 -p 2222 root@galilee.eedf.fr
